# OWI CLI

The OWI CLI is in heavy development on [the project gitlab](https://opencode.it4i.eu/openwebsearcheu-public/owi-cli).

## Preparation

### Lexis Integration

Py4Lexis has problems with Python 3.12, so setup an environment for 3.11:

    poetry env use python3.11
    poetry shell

Next, install the `py4lexis` package:

    pip install py4lexis --index-url https://opencode.it4i.eu/api/v4/projects/107/packages/pypi/simple

Subsequently, install the OWI CLI:

    poetry install --no-root

... and its plugins (i.e., DuckDB for now):

    poetry install -E plugins

### Python3.12 

If you install Py4Lexis and the Python iRods client locally, you can fix things for 3.12:

See [this issue comment](https://opencode.it4i.eu/lexis-platform/clients/py4lexis/-/issues/10#note_5129).

## Usage

Basic listing of available files:

    owi ls
    owi ls BAdW-LRZ:2023-11-29#0/Access=public

Slow, but complete info:

    owi ls BAdW-LRZ:2023-11-29#0/Access=public --details

Reset credentials etc.:

    owi clean

Open an interactive Python shell with DuckDB integration:

    owi duckdb ...


