--
-- Examples of DuckDB queries over OWS.EU crawl data on S3
--

-- Preliminaries: 
--   set the environment variables by sourcing s3-env.sh

-- Extra configuration
INSTALL httpfs;
LOAD httpfs;
--SET s3_url_style='path';
--SET s3_region='us-east-1';

-- What URLs were seeded to the crawler?
SELECT * FROM read_csv_auto('s3://ows/test-arjen/urls.txt');

-- What metadata exists?
-- DESCRIBE FROM read_parquet('s3://ows/test-arjen/data/metadata.parquet.gz');

-- What URLs were crawled, and when?
SELECT warc_date, url, language FROM read_parquet('s3://ows/test-arjen/data/metadata.parquet.gz');

-- Metadata sample from the index on April 1st, 2024
SELECT * FROM read_parquet('s3://ows/index/year=2024/month=4/day=1/language=eng/metadata_33.parquet') LIMIT 10;

-- Check duplicate docids
create view metadata as from read_parquet('s3://ows/index/year=2024/month=4/day=1/language=eng/metadata_*.parquet');
select id, record_id, warc_date from metadata where id in (SELECT id FROM metadata GROUP BY id HAVING count(id) > 1);

