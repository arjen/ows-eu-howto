#!/bin/bash
export DUCKDB_S3_USE_SSL=1
vns=(DUCKDB_S3_ENDPOINT AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY)
vvs=($(jq -r '.aliases["ows-it4i"]|.url,.accessKey,.secretKey|split("//")|last' $HOME/.mc/config.json))
for i in "${!vns[@]}"
do 
  export "${vns[$i]}=${vvs[$i]}"
done
