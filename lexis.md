# Setup Lexis and Airflow

## VPN

You will need an IT4I login, your public key should be known at IT4I, and you need a password.
Request setting this all up through email to support@lexis.tech.

IT4I provides [instructions for VPN access](https://docs.it4i.lexis.tech/_pages/vpn/vpn.html) for developers.

This requires on FC41 installation of the SSTP extension for NetworkManager:

    sudo dnf install NetworkManager-sstp-gnome 

We may also need (TODO):

    sudo dnf install sstp-client

You need the following settings:

    Server  vpn.it4i.lexis.tech
    DNS     172.16.100.172, 172.16.100.173
    Route   176.16.0.0 - netmask 16 - gateway 192.168.210.1

In the terminal, execute as root the following commands:

    resolvectl domain ppp0 "~msad.it4i.lexis.tech"
    resolvectl dns ppp0 172.16.100.172 172.16.100.173

Alternatively (when using Gnome NetworkManager), edit script [`start-vpn-it4i.sh`](start-vpn-it4i.sh) for the
UUID value as shown with `nmcli con` and copy it to the directory with dispatcher scripts as follows:

    sudo cp start-vpn-it4i.sh /etc/NetworkManager/dispatcher.d/

## Airflow

See [airflow-doc] or the [PDF](OWS Apache Airflow.pdf) saved in this repo.

Credentials for the [Airflow Web UI](http://airflow-ows.msad.it4i.lexis.tech/home) are `airflow` and `openwebindex`.

E.g., view [cluster activity](http://airflow-ows.msad.it4i.lexis.tech/cluster_activity).

TODO

## References

[airflow-doc]:  https://syncandshare.lrz.de/open/MjhOV25UbUc0Y1VyNnB6NG5jY2Uz/Workpackages/WP5/LEXIS%20Workflows/OWS%20Apache%20Airflow.docx    "Airflow doc on Sync and Share"
