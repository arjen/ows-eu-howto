# OWI

## Index Access

### Install OWILIX

    git clone https://opencode.it4i.eu/openwebsearcheu-public/owi-cli.git
    cd owi-cli/
    poetry install    

### Configure OWILIX

Setup Index Repository

    mkdir /export/data2/arjen/OWS.EU/owi
    export OWS_OWI_PATH=/export/data2/arjen/OWS.EU/owi

Not sure if everything is correct;
but I had to add owilix explicitly to the path after running `poetry shell`.

    poetry shell
    export PATH=/home/arjen/.cache/pypoetry/virtualenvs/owilix-z1hRtEHO-py3.10/bin/:$PATH

### Using OWILIX

    owilix remote pull it4i:latest/access=public

Agree to the license, and agree to downloading a lot of data, and there you go.


