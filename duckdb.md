# DuckDB as a search application

## Introduction

OWS develops an example search application in Java, using a mix of Lucene and JDBC to query the Parquet metadata.

DuckDB provides a viable alternative by moving the query processing part completely to SQL.

Only a very small amount of front-end code would be needed to create the UI/UX, even for complex query needs.
Perhaps, all we need can be expressed in HTML + CSS only?!

This post uses the index and data resulting from a small example crawl with a small number of my own Web pages 
as the crawler's seed list.

## Loading metadata

The OWS crawling pipeline parses and enriches the collected Web data, writing the metadata into Parquet.

Let's load the metadata into a table:

    CREATE TABLE ciff_metadata AS SELECT * FROM read_parquet('/export/data/ir/OWS.EU/data/metadata.parquet.gz');

Describe the schema information:

    DESCRIBE ciff_metadata;

A simple but useful sample query would report the URL of an indexed document:

    SELECT url FROM ciff_metadata
    WHERE record_id = '4eb9a12b-b478-4cb9-b471-84ab89a0a2d8';

You may also directly query the Parquet file without first loading everything into a DuckDB table:

    SELECT url 
    FROM read_parquet('/export/data/ir/OWS.EU/data/metadata.parquet.gz') 
    WHERE record_id = '4eb9a12b-b478-4cb9-b471-84ab89a0a2d8';

A more complex usecase would determine the captured URLs that should be added to the frontier:

    create temp table out_links as select unnest(outgoing_links) url from ciff_metadata;
    select distinct url from out_links where url not in (select url from ciff_metadata);

_Ideally, the temporary table would not be needed, but the current implementation of the SQL binder 
does not allow using the result of unnesting the list in a join subquery._

## FTS

The DuckDB Full Text Search extension can provide ranking over the `plain_text` column of the metadata:

    PRAGMA create_fts_index('ciff_metadata','record_id','plain_text');

You can add additional fields as well, just extend the list of fields that you expect to query.

Let us rank the `plain_text` documents using BM25:

    SELECT record_id, title, url, fts_main_ciff_metadata.match_bm25(
        record_id,
        'big data'
    ) AS score
    FROM ciff_metadata 
    WHERE score IS NOT NULL
    ORDER BY score DESC 
    LIMIT 10;

As we are just using SQL, we can add other constraints, e.g. for ranking English about multimedia:

```
D SELECT record_id, title, url, fts_main_ciff_metadata.match_bm25(
        record_id,
        'multimedia'
    ) AS score
    FROM ciff_metadata
    WHERE score IS NOT NULL AND language = 'eng' 
    ORDER BY score DESC;
┌──────────────────────────────────────┬─────────────────────────────────────────────┬─────────────────────────────────────────────────────────────┬────────────────────┐
│              record_id               │                    title                    │                             url                             │       score        │
│               varchar                │                   varchar                   │                           varchar                           │       double       │
├──────────────────────────────────────┼─────────────────────────────────────────────┼─────────────────────────────────────────────────────────────┼────────────────────┤
│ 6e0bc739-513a-42dd-bc9e-4c6843f20457 │  PhD Thesis - Arjen P. de Vries             │ https://arjenp.dev/phd/abstract_en.html                     │ 3.7941749376023166 │
│ 85b2af1b-90c3-4b44-a7fd-ce8faa44f15e │  PhD Thesis - Arjen P. de Vries             │ https://arjenp.dev/phd/                                     │  3.421200729980732 │
│ 680240e8-0e75-4df9-bb66-3ad575997bbd │ Linking data sources with Spinque - Spinque │ https://spinque.com/blog/linking-data-sources-with-spinque/ │ 2.8266490361211027 │
└──────────────────────────────────────┴─────────────────────────────────────────────┴─────────────────────────────────────────────────────────────┴────────────────────┘
```

Or the Dutch ones, as easily:

```
D SELECT record_id, title, url, fts_main_ciff_metadata.match_bm25(
        record_id,
        'multimedia'
    ) AS score
    FROM ciff_metadata
    WHERE score IS NOT NULL AND language = 'nld' 
    ORDER BY score DESC;
┌──────────────────────────────────────┬──────────────────────────────────┬─────────────────────────────────────────┬────────────────────┐
│              record_id               │              title               │                   url                   │       score        │
│               varchar                │             varchar              │                 varchar                 │       double       │
├──────────────────────────────────────┼──────────────────────────────────┼─────────────────────────────────────────┼────────────────────┤
│ 1195a9bd-fe4d-47fe-90be-82b8136cb8bc │  PhD Thesis - Arjen P. de Vries  │ https://arjenp.dev/phd/abstract_du.html │ 3.7130765253567337 │
└──────────────────────────────────────┴──────────────────────────────────┴─────────────────────────────────────────┴────────────────────┘
```

The tables for the FTS index are part of a separate schema:

    SELECT table_name FROM duckdb_tables() WHERE schema_name = 'fts_main_ciff_metadata';

A difference with the postings read from a CIFF file is that the FTS `terms` table contains the original term sequence;
not yet aggregated into term frequency counts. This representation may be useful for processing phrase queries, but is 
lost in the CIFF representation.

So, the terms of  document 0 after stemming and stopping can be reconstructed from the FTS tables as follows:

    SELECT dict.term, terms.termid 
    FROM fts_main_ciff_metadata.terms 
      JOIN fts_main_ciff_metadata.dict 
      ON terms.termid = dict.termid 
    WHERE terms.docid=0;

Another difference between the CIFF tables and DuckDB's FTS index is that the latter defines by default a fielded index.

## Loading a CIFF index 

We will use a schema `ows` and use it below.

    -- CREATE SCHEMA ows;
    USE ows;

Refer to the CIFF2DuckDB repo for actually loading the tables from CIFF.

## Querying the CIFF index

We can simply redefine the `bm25` macro to use the term frequencies from the `postings` table.

First, ensure that tokenize exists in the current schema:

    CREATE OR REPLACE MACRO tokenize(s) AS fts_main_ciff_metadata.tokenize(s);

__Note:__ CIFF does not define a tokenizer, and we should fix this following our OSSYM paper.

CIFF does not define fields (yet?!), so we define a default field `doc` ourselves:

    CREATE OR REPLACE TABLE fields (fieldid BIGINT, field VARCHAR);
    INSERT INTO fields VALUES (0, 'doc');

Subsequently, redefine the BM25 macro:

```
CREATE OR REPLACE macro match_bm25(docname, query_string, fields := NULL, k:=1.2, b:=0.75, conjunctive:=0, stemmer:='none') AS (
            WITH tokens AS (
                SELECT DISTINCT stem(unnest(tokenize(query_string)), stemmer) AS t
            ),
            fieldids AS (
                SELECT fieldid
                FROM ows.fields
                WHERE CASE WHEN fields IS NULL THEN 1 ELSE field IN (SELECT * FROM (SELECT UNNEST(string_split(fields, ','))) AS fsq) END
            ),
            qtermids AS (
                SELECT termid
                FROM dict AS dict,
                     tokens
                WHERE dict.term = tokens.t
            ),
            qterms AS (
                SELECT termid,
                       docid,
                       tf
                FROM (SELECT *, 0::BIGINT AS fieldid FROM postings) AS postings
                WHERE CASE WHEN fields IS NULL THEN 1 ELSE fieldid IN (SELECT * FROM fieldids) END
                  AND termid IN (SELECT qtermids.termid FROM qtermids)
            ),
            cdocs AS (
                SELECT docid
                FROM qterms
                GROUP BY docid
                HAVING CASE WHEN conjunctive THEN COUNT(DISTINCT termid) = (SELECT COUNT(*) FROM tokens) ELSE 1 END
            ),
            subscores AS (
                SELECT docs.docid,
                       len,
                       qterms.termid,
                       tf,
                       df,
                       (log(((SELECT num_docs FROM stats) - df + 0.5) / (df + 0.5))* ((tf * (k + 1)/(tf + k * (1 - b + b * (len / (SELECT avgdl FROM stats))))))) AS subscore
                FROM qterms,
                     cdocs,
                     docs AS docs,
                     dict AS dict
                WHERE qterms.docid = cdocs.docid
                  AND qterms.docid = docs.docid
                  AND qterms.termid = dict.termid
            ),
            scores AS (
                SELECT docid,
                       sum(subscore) AS score
                FROM subscores
                GROUP BY docid
            )
            SELECT score
            FROM scores,
                 docs AS docs
            WHERE scores.docid = docs.docid
              AND docs.name = docname
        );
```

Now you can query the collection using its CIFF index:

    SELECT record_id,title,score 
    FROM (
      SELECT *,match_bm25(record_id, 'big data') AS score 
      FROM ciff_metadata
    ) sq 
    WHERE score IS NOT NULL 
    ORDER BY score DESC 
    LIMIT 10;

Or, as above but now using the CIFF index, rank the Dutch documents about 'multimedia':

    SELECT record_id, title, url, fts_main_ciff_metadata.match_bm25(
        record_id,
        'multimedia'
    ) AS score
    FROM ciff_metadata
    WHERE score IS NOT NULL AND language = 'nld'
    ORDER BY score DESC;


