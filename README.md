# OWS.EU HOWTO

Arjen's personal notes related to [OWS.EU](https://ows.eu/).

## Preliminaries

How to use [B2Access](b2access.md)?

Project [gitlab](https://opencode.it4i.eu/) and [sign in](https://opencode.it4i.eu/users/sign_in).

Mattermost [invite link](https://openmm.it4i.eu/signup_user_complete/?id=ejn35iyrwtd5mcacboy6cg68ue).

## Development

How to use [S3](S3.md)?

How to install and use the [OWI CLI](owscli.md) (Owilix)?

How to use [Lexis and Airflow](lexis.md)?

