Clone repo:

    git clone git@github.com:ArchiveTeam/wget-lua.git

Install prerequisites:

    sudo dnf install autoconf-archive
    sudo dnf install gnutls gnutls-devel
    sudo dnf install compat-lua compat-lua-devel

Edit `configure.ac` to replace `lua5.1` by `lua-5.1` (this is probably Fedora specific).

For editing the `configure.ac` file, `pkg-config` needs the path to `/usr/lib64/pkgconfig/lua-5.1.pc`, and then provides the info necessary, e.g.:

    pkg-config --cflags  /usr/lib64/pkgconfig/lua-5.1.pc
    pkg-config --libs  /usr/lib64/pkgconfig/lua-5.1.pc

_Most likely, this should really be handled with modifying the M4 setup, but I do not know._

Then, we can install:

    ./bootstrap
    ./configure
    make
