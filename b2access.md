# B2ACCESS

## Register

First login on [b2access homepage](https://b2access.eudat.eu/home/) using the _Radboud University_ organisation.

Then you can register a "personal" account, add a password in the credentials tab (note these are organised vertically!).
The account name will be your `U-number@ru.nl`.

Note that if the b2access does not support your home institution, you can login with other providers like ORCID (or other OAUTH2 providers).

